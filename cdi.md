# Contrat de travail à durée indéterminée

*Entre*

Codeurs en Liberté, société par actions simpifiée au capital de 4000 euros, inscrite au R.C.S.
de Paris sous le numéro 821611431, dont le siège social est situé au 19A Square de
Monsoreau 75020 Paris, représentée par Tristram Gräbener dument habilité,
(ci-après désigné l’ « **Employeur** »), *d’une part*,

*Et* la personne employée :

`________`, résidant `_______` de nationalité `_________` né·e le
`________`, à `_______`,

(ci-après désignée la « **Personne** »), *d’autre part*,

(ci-après collectivement désignés les « **Parties** »),

il a été convenu ce qui suit :

## Article 1 : Engagement

Le présent contrat de travail est soumis aux conditions indiquées ci-après ainsi qu’aux
dispositions légales et réglementaires.

La Personne déclare être libre à cette date de tout autre engagement professionnel
incompatible avec ce contrat.

Toute fausse déclaration à cet égard exposera la Personne à réparer tout préjudice résultant
de poursuites d’un ancien employeur ou toute personne à qui la Personne est liée
contractuellement, ceci en application de l’article L.1237-3 du Code du travail.

Le présent contrat deviendra définitif sous réserve des résultats de la visite médicale
d’embauche décidant de l’aptitude de la Personne au travail proposé.

## Article 2 : Statut

La Personne exercera la fonction d'Ingénieur, statut « Cadre ».
En cette qualité, la Personne devra notamment accomplir les tâches suivantes : conseil en
informatique et démarchage commercial.

Le détail des fonctions de la Personne et l'étendue spécifique de ses attributions seront
déterminés et modifiés par l’Employeur en fonction du développement de l'activité et
des besoins de l’Employeur.

## Article 3 : Période d’essai

Le présent contrat deviendra définitif à l'expiration d'une période d'essai de 4 mois à
compter de la date de prise des fonctions, fixée le `_____`. Pendant la période d'essai,
chacune des Parties pourra mettre fin au présent contrat conformément à la loi
applicable.

## Article 4 : Durée du contrat et préavis

Le présent contrat est conclu pour une durée indéterminée à compter du : `______`.

En cas de rupture de la période d’essai, le présent contrat prendra fin après la période de
préavis telle que prévue par les dispositions légales (et, le cas échéant, conventionnelles)
applicables.
Chacune des Parties pourra mettre fin au présent contrat, sous réserve de respecter le
préavis tel que fixé par les dispositions légales (et, le cas échéant, conventionnelles) en
vigueur.

Il est rappelé que ce préavis ne sera pas dû en cas de licenciement pour faute
grave ou lourde de la Personne ou en cas de rupture conventionnelle.

La partie prenant
l'initiative de la rupture du présent contrat aura l'obligation d'informer l'autre partie de
sa décision, par lettre recommandée avec accusé de réception en application des
dispositions légales sauf cas de rupture conventionnelle telle que prévue aux articles L.
1237-11 et suivants du Code du travail.

## Article 5 : Lieu de travail et déplacements professionnels

La Personne travaille intégralement en télétravail là où elle le souhaite.

## Article 6 : Durée du travail

La Personne exercera ses fonctions à hauteur de 30 heures de travail
effectif hebdomadaire.

## Article 7 : Objectifs

La Personne s’engage à générer 6000 (six mille) euros de chiffre d’affaires par trimestre.

## Article 8 : Rémunération

### Article 8.1 : Rémunération fixe
En rémunération de ses services, la Personne percevra une rémunération fixe forfaitaire
brute mensuelle de 1250 (mille deux cent cinquante) euros.

La rémunération de la Personne sera payable en 12 mensualités égales.

### Article 8.2 : Rémunération variable
En cas de dépassement des objectifs trimestriels, une prime de performance
est versée. Elle représente _a minima_ 50% du volume excédentaire généré.

La répartition exacte est fixée par le règlement intérieur.

### Article 8.3 : Frais professionnels

Les frais professionnels, à savoir les frais de transport et de séjour, qui seront engagés
par la Personne pour l’exercice normal de ses fonctions et suivant des instructions qui lui
auront été données, seront pris en charge par l’Employeur, sur présentation des
justificatifs habituels dans les conditions actuellement fixées par le règlement intérieur et dont la
Personne a pris connaissance.

Une compensation forfaitaire de 100 (cent) euros mensuels couvrent les frais
liés au télétravail, à savoir : aménagement d’un espace de travail, repas,
électricité et accès internet.

## Article 9 : Exclusivité et confidentialité

La Personne n’est pas tenue à une confidentialité particulière, sauf concernant
les données suivantes, qui doivent rester confidentielles à moins d'un accord
explicite de toutes les parties qu'elles concernent :

- informations sur des négociations commerciales pas encore conclues ;
- données personnelles.

## Article 10 : Dispositions finales

Le présent contrat annule et remplace tout autre écrit ou précédent accord verbal entre les Parties.
Si à un moment quelconque après la date des présentes, une disposition du présent
contrat était déclarée illicite, nulle ou non opposable, elle serait sans
effet, mais l’illégalité, nullité ou inopposabilité de cette disposition n'affectera pas la validité et
l'opposabilité des autres dispositions du présent contrat.

Fait à Paris, le `_______`
En deux originaux (un pour chaque signataire)

Codeurs en Liberté, représentée par Tristram Gräbener,

La Personne `__________`
