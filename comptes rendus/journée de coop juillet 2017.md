#Journée de coopérative - compte-rendu

## Discussion sur le tableau des comptes partagés (renommé "Ardoise")

### Décisions :

   * Le CA saisi est celui qui est facturé, pas nécessairement encaissé

### Douleurs actuelles

   * devoir retaper des infos dans plusieurs endroits (zefyr, et le tableur)
   * trop d'informations à un même endroit
   * trop petit (mauvaise mise en forme)
   * Compliqué de calculer la prime de chaque mois
   * Pas compris comment renseigner les frais
   * Difficile de rajouter un sociétaire (il faut dupliquer les colonnes)
   * conflits trop faciles avec owncloud
   * Difficile d'accès (aller dans owncloud, trouver le bon répertoire, télécharger le fichier …)
   * J'ai l'impression qu'il y a des informations dont on ne se sert pas

### Objectifs

   * Calcul du salaire en fonction du CA de chacun
       * Cumul de l’état de chacun (Ardoise)
   * Calcul de la contribution au cookies
   * Savoir si on a des sous en collectif
   * Avoir un modèle réplicable
   * Prise en main simple pour les nouveaux entrants
   * Connaitre nos frais de fonctionnement
   * Respect de la vie privée / confidentialité des données
   * Avoir la maîtrise de l’état des comptes

### Pistes envisagées

   * Tableur sur owncloud
   * Tableur sur cloud web (icloud, google drive, framacalc, …)
   * CSV (sur git?) avec CLI (ou chatbot)
   * CLI qui scrappe Zefyr
   * App

### Discussion sur le pot à cookies

Objectif :

   * Ne sert que à payer des salaires (pas de dons…)
   * Doit servir à financer des communs (open source, données, doc, bénévolat…)
   * Ne doit pas servir directement à Codeurs en Liberté
   * Une bourse de 1000€ à chaque fois

## Qu'est-ce qu'on veut être ?

### Qu'est-ce qui fonctionne bien en ce moment ?

**Vincent** : content de ne plus être en CDD, ni au RSI. Content de la dimension humaine ; content de l'autogestion, plutôt que de payer des fonctions support. Difficulté à se retrouver autour d'un projet commun ; vu à l'incubateur, l'émergence de projets comme ça, ça prend du temps.

**Tristram** : content de montrer qu'on peut gérer une boite nous-même, faire la compta. Content d'avoir une structure pour facturer. Aime bien l'idée qu'on puisse financer des projets annexes à terme (assos, volontariat). Envie de rester à Codeurs en Liberté, mais tiraillé sur l'avenir : contribuer à transport.data.gouv.fr, travailler avec le STIF, le projet de l'ancien patron de CanalTP ; est-ce que je peux faire tout ça en même temps ? Envie aussi de faire un projet concret (par exemple un produit à nous). Envie d'un espace physique pour se voir.

**Thimy** : contente d'un accueil pour démarrer en freelance. Contente d'être avec nous tous ; de la liberté de faire plus ce que je veux, et d'élargir au design. Contente d'apprendre beaucoup. Dommage qu'on soit souvent ensemble, mais sans l'être vraiment.

**Nicolas** : content d'être à nouveau en indépendant sans repartir dans le délire du RSI. Content de voir que gérer une boite n'est pas si compliqué que ça. Content d'avoir un emploi du temps moins chargé que chez Trainline (4/5ème, et les temps de creux entre les contrats). Le futur immédiat va être chez GeoVélo, pendant potentiellement un moment. Envie de s'éloigner du développement iOS ; sans doute à un moment de mettre les pieds chez beta.gouv.fr. Envie de passer du temps sur l'appli Bicyclette. Pour un projet commun : se retrouver tous chez un client, travailler sur notre propre projet ? Je ne sais pas. En tout cas le cadre de Codeurs en Liberté est complètement adapté à ça.

**Pierre** : content des copains, content de rencontrer plein de personnes différentes, de l'autogestion, content du rythme de travail allégé. Envie de passer plus de temps à travailler ensemble : avoir un espace physique, avoir des projets communs à plusieurs ?

### Projets communs

Les projets communs, ça ne serait pas forcément un gros projet unique de toute la coopérative : ça peut déjà être passer du temps à travailler sur nos outils internes.

Tristram parle de ce projet avec un ancien de CanalTP : calculer tous les itinéraires de transports en commun d'une région, croiser les données avec les bassins d'habitation et d'emploi, et qualifier les zones sous-desservies. Ça serait un projet ponctuel d'environ trois mois, à compétences transverses (algo + interface de présentation de données). Ça reste assez hypothétique pour l'instant.

Également cette idée d'un projet "le.covoiturage", une API pour fédérer les acteurs du covoiturage et les aider à atteindre une masse critique. Ça pourrait se faire dans le cadre de beta.gouv.fr, ou du STIF.

GéoVélo pourrait également avoir besoin de monde pour des besoins divers.

Tout ça c'est plutôt des idées de prospects que des projets communs potentiels. La question est plutôt "Si un client demande à ce que ça soit fait dans trois mois", est-ce qu'on a des gens disponibles pour staffer ? Ou est-ce qu'on fait tourner autour de nous / sur le Slack de l'incubateur ?

Conclusion : ça serait chouette d'être plusieurs à bosser sur le même projet, mais on ne va pas le chasser non plus. Si une offre nous tombe dessus, on avise.

### Avoir des locaux en communs

ut7 fait une "journée de coop" dans leurs locaux chaque jeudi : le matin ils parlent de la coop, l'après-midi ils invitent des gens. On peut essayer de louer des locaux une fois par semaine avec Copass.

On va se renseigner sur Copass.

### Accueillir de nouveaux collègues

C'est difficile d'intégrer des gens rapidement. On veut sans doute y aller doucement.

Il y a des gens qu'on connait déjà, et on peut voir au cas par cas. Il y a également le cas de C., rencontrée à Pas Sage en Seine, qui avait l'air intéressée par Codeurs en Liberté : aucun d'entre nous n'a jamais passé de temps avec elle.

Les autres coopératives sont souvent petites (ut7 a cinq personnes, Scopyleft trois). Pourquoi ? Est-ce que c'est lié à la manière dont ils se paient, ou dont ils fonctionnent ?

Est-ce qu'on est ouvert à des gens qui ne sont pas des développeurs ? D'un côté ça serait bien pour la diversité des profils. Ça pourrait aussi être bien pour faire des projets ensemble. D'un autre côté les risques financiers sont plus faibles pour les codeurs.

Conclusion : ça sous-entend probablement une affinité d'état d'esprit, et une profession dans les métiers du numérique. Dans ce cadre là on serait ouvert à d'autres professions que les codeurs purs et durs.

Incidemment, il y a consensus sur le fait qu'on ne cherche pas à recruter activement.

## Bilan

C’était super chouette – et aussi vraiment trop court. On a envie de remettre ça une autre fois, en y passant plus de temps ; surtout maintenant qu'on s'est rendu compte que la logistique était finalement assez facile.

