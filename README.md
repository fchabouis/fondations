# Introduction

Ce projet regroupe les documents publics de Codeurs en Liberté : statuts, compte-rendus, modèles de contrat, guides des opérations courantes, etc.

# Orientation

- **Pour se renseigner**, consulter le **[wiki](https://gitlab.com/CodeursEnLiberte/fondations/wikis/home)**.
- **Pour discuter**, ouvrir une **[issue](https://gitlab.com/CodeursEnLiberte/fondations/issues)**.
- **Pour les documents archivés**, voir les **[fichiers](https://gitlab.com/CodeursEnLiberte/fondations/tree/master)**.
