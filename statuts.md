Codeurs en Liberté

Société par actions simplifiée à capital variable

Capital : 4000 euros

Siège social : 19 A Square de Monsoreau 75020 Paris

Société par actions simplifiée à capital variable

# Status constitutfis

Les soussignés :

* `_______`,
* `_______`,
* `_______`.

Ont établi ainsi qu'il suit les statuts d'une société par actions simplifiée et désigné les premiers dirigeants de ladite société (la « Société »).


## Article 1 : Forme de la Société
Il est constitué par les présentes, sous la forme d’une société par actions simplifiée à capital variable, une société qui sera régie par les dispositions législatives et réglementaires en vigueur notamment les dispositions des articles L. 227-1 et suivants du code de commerce ainsi que par les présents statuts (les « **Statuts** »).

La Société fonctionne indifféremment sous la même forme avec un ou plusieurs associés.

En cas de réunion de toutes les actions en une seule main, l'associé unique exerce seul les pouvoirs dévolus à la collectivité des associés par les Statuts.

Elle ne peut procéder à une offre au public de titres financiers.

## Article 2 : Objet
La Société a pour objet, tant en France qu'à l'étranger :

La formation, étude, conseil, prototypage et développement en informatique.
L'objet social inclut également, plus généralement toutes opérations économiques, juridiques, industrielles, commerciales, civiles, financières, mobilières ou immobilières se rapportant directement ou indirectement à son objet social (y compris toute activité de conseil se rapportant directement ou indirectement à l’objet social), ou tous objets similaires, connexes ou complémentaires ou susceptibles d’en favoriser l’extension ou le développement.

La Société peut agir, tant en France qu'à l'étranger, pour son compte ou pour le compte de tiers, soit seule, soit en participation, association, groupement d'intérêt économique ou société, avec toutes autres sociétés ou personnes et réaliser, sous quelque forme que ce soit, directement ou indirectement, les opérations rentrant dans son objet.

La Société peut également prendre, sous toutes formes, tous intérêts et participations dans toutes affaires et entreprises françaises et étrangères, quel que soit leur objet.

## Article 3 : Dénomination
La dénomination sociale de la Société est Codeurs en Liberté.

Tous les actes et documents, émanant de la Société et destinés aux tiers doivent indiquer la dénomination sociale précédée ou suivie immédiatement et lisiblement des mots _société par actions simplifiée à capital variable_ ou de _SAS à capital variable_, et de l'énonciation du montant du capital social, du lieu et du numéro d’immatriculation de la société au registre du commerce et des sociétés.

## Article 4 : Siège social
Le siège social est fixé au 19 A Square de Monsoreau 75020 Paris.
Il peut être transféré en tout autre lieu en France Métropolitaine par simple décision du Président.

Le siège social peut également être transféré en tout lieu en vertu d'une décision collective des associés ou par décision de l’associé unique.
Lors d'un transfert décidé par le Président, celui-ci est autorisé à modifier les Statuts en conséquence.

Des agences, succursales et dépôts peuvent être créés en tous lieux et en tous pays par simple décision du Président, qui peut ensuite les transférer et les supprimer comme il l'entend.

## Article 5 : Durée

La durée de la Société est fixée à quatre-vingt-dix-neuf (99) années, à compter de la date de son immatriculation au registre du commerce et des sociétés, sauf décision de dissolution anticipée ou prorogation.

## Article 6 : Exercice social

L'exercice social a une durée de 12 mois, commence le 01 janvier et se termine le 31 décembre de chaque année.

Exceptionnellement, le premier exercice commencera à courir à compter du jour de l'immatriculation de la Société au registre du commerce et des sociétés et sera clos le 31 décembre 2017.

## Article 7 : Apports
À la constitution de la Société, le(s) soussigné(s) font apport à la Société de la somme de 4000 euros correspondant à 4000 actions d'une valeur nominale de 1.0 €.

Les actions ont été souscrites et libérées en totalité.

Les fonds correspondants ont été déposés sur le compte ouvert au nom de la Société en formation auprès de la banque Bred Banque Populaire, ainsi qu’il résulte du certificat établi préalablement à la signature des Statuts par la banque dépositaire des fonds, sur présentation de l'état de souscription mentionnant la somme versée par les associés et certifiée sincère et véritable par le Président.

## Article 8 : Capital social
### Article 8.1 : Capital social souscrit
Le capital social souscrit est fixé à la somme de 4000 euros.
Il est divisé en 4000 actions de 1.0 € de valeur nominale chacune, toutes de même catégorie, intégralement souscrites à la constitution.

### Article 8.2 : Variabilité du capital social
Le montant du capital social pourra être augmenté par le biais de versements successifs des associés ou de l'admission de nouveaux associés.

Le montant du capital social pourra être réduit par la reprise totale ou partielle des apports effectués par les associés de la Société.

Toutefois, toute augmentation de capital par apport en nature, comme toute réduction de capital par reprise d'éléments d'actifs, devra être réalisée dans les conditions fixées par l'article 9 ci-après.

Le capital minimum est fixé à la somme de 1000 euros.

Le capital maximum autorisé est fixé à la somme de 100000 euros.

Toute augmentation du capital social par incorporation de réserves, primes ou bénéfice devra être décidée par la collectivité des associés de la Société.

La réduction du capital social pour cause de pertes ou diminution de la valeur nominale des actions émises par la Société relève d'une décision collective prise aux conditions de quorum et de majorité nécessaires pour la modification des statuts.

Le dernier jour de chaque trimestre civil, il sera fait le compte des souscriptions reçues par la Société au cours du trimestre écoulé qui feront alors l'objet d'une déclaration récapitulative de souscription et de versement.


## Article 9 : Modifications du capital social
Le capital social peut être augmenté ou réduit dans les conditions prévues par la loi, par décision collective des associés prise dans les conditions visées à l’article 14 ci-après ou par décision de l’associé unique.

Les associés peuvent déléguer au Président les pouvoirs nécessaires à l’effet de réaliser, dans le délai légal, l’augmentation de capital en une ou plusieurs fois, d’en fixer les modalités, d’en constater la réalisation et de procéder à la modification corrélative des statuts.

Le capital doit être intégralement libéré avant toute émission d’actions nouvelles à libérer en numéraire, à peine de nullité de l’opération.

En cas d’émission d’actions nouvelles, les actions sont émises soit à leur montant nominal, soit à ce montant majoré d’une prime d’émission.

Les actions nouvelles sont libérées soit en espèces, soit par compensation avec des créances certaines, liquides et exigibles sur la Société, soit par incorporation de réserves, bénéfices ou primes d'émission, soit par apport en nature, soit par fusion ou scission, soit par tout autre mode prévu par la loi.

Les associés ont, proportionnellement au nombre de leurs actions, un droit de préférence à la souscription des actions en numéraire émises pour réaliser une augmentation de capital.

Les associés peuvent supprimer le droit préférentiel de souscription en faveur d’une ou plusieurs personnes dénommés dans le respect des conditions prévues par la loi. En outre, les associés peuvent renoncer à titre individuel à leur droit préférentiel de souscription.

Les associés peuvent autoriser la modification du capital et déléguer au Président les pouvoirs nécessaires à l’effet de la réaliser.

Toute modification du capital social doit garantir que au moins 51% du capital et 51% des voix soient détenues par des employés ou assimilés employés. Le
président est assimilé employé, même s’il n’est pas rémunéré.

## Article 10 : Forme des actions
Les actions ont la forme nominative.

Elles donnent lieu à une inscription à un compte ouvert par la Société au nom de l'associé dans les conditions et selon les modalités prévues par la loi et les règlements en vigueur

Il peut être créé des actions de préférence, avec ou sans droit de vote, assorties de droits particuliers de toute nature, à titre temporaire ou permanent, dans le respect des dispositions légales en vigueur.

## Article 11 : Droits et obligations attachés aux actions
Chaque action donne droit dans les bénéfices, l'actif social et le boni de liquidation à une part proportionnelle à la quotité du capital qu'elle représente.

Elle donne en outre le droit au vote et à la représentation lors des décisions collectives, ainsi que le droit d'être informé sur la marche de la Société et d'obtenir communication de certains documents sociaux aux époques et dans les conditions prévues par la loi et les statuts. Chaque détenteur a une voix lors des décisions collectives, quel que soit le nombre d’action détenues.

Les associés ne sont responsables du passif social qu'à concurrence de leurs apports.

Les droits et obligations suivent l'action quel qu'en soit le titulaire.

Les actions sont indivisibles à l’égard de la Société. Les copropriétaires indivis d’actions sont représentés par l’un deux ou par un mandataire commun de leur choix. A défaut d’accord entre eux sur le choix du mandataire, celui-ci est désigné par ordonnance du Président du tribunal de commerce statuant en référé à la demande du copropriétaire le plus diligent.

Le droit de vote attaché à l’action appartient au nu-propriétaire, sauf pour les décisions collectives relatives à l’affectation des bénéfices de la Société où il appartient à l’usufruitier. Dans tous les cas, le nu-propriétaire peut participer aux décisions collectives même à celles pour lesquelles il ne jouit pas du droit de vote.

Le droit de l’associé d’obtenir communication de documents sociaux ou de les consulter peut également être exercé par chacun des copropriétaires d’actions indivises, par l’usufruitier et le nu-propriétaire d’actions.

La propriété d'une action comporte de plein droit adhésion aux statuts de la Société et aux décisions collectives des associés ou de l’associé unique.


## Article 12 : Modalités de transmission des actions
Tout transfert d’actions au profit de quiconque sera soumis au droit de préemption préalable des associés dans les conditions suivantes.
En cas de projet de transfert de tout ou partie de ses actions par un associé au profit de quiconque, les autres associés auront la possibilité d’acquérir la totalité (et non une partie seulement) desdites actions en lieu et place du cessionnaire envisagé, aux mêmes conditions de prix ou de valorisation que celles offertes par ledit cessionnaire.

Tout associé ayant pour projet un transfert d’actions de la Société devra le notifier aux autres associés et à la Société avec l'indication
* (i) de l’identité et de l’adresse du cessionnaire envisagé,
* (ii) du nombre d’actions concernées,
* (iii) de la nature de l’opération projetée (vente, apport, etc.)
* (iv) du prix - ou de la valorisation si l’opération n’est pas une vente pure et simple - des actions ainsi que des modalités de paiement.

Chacun des autres associés (en ce compris le cessionnaire envisagé, s’il est déjà associé) disposera alors d’un délai de trente (30) jours pour notifier sa volonté d’exercer son droit de préemption (et le nombre d’actions qu’il souhaite préempter) selon les modalités suivantes :

le droit de préemption ne pourra être valablement exercé que si toutes les actions objet du projet de transfert sont préemptées du fait de cet exercice ;

En cas d’exercice du droit de préemption, le prix d’achat des actions préemptées sera égal au prix ou à la valorisation énoncé(e) dans la notification de projet de transfert adressée par l’associé cédant ;

Si le nombre total d’actions que les autres associés souhaitent acquérir est supérieur au nombre d’actions objet du projet de transfert, chaque associé ayant exercé son droit de préemption (un « Préempteur ») verra sa demande satisfaite dans la limite du droit reconnu à chacun des autres Préempteurs d’acquérir un nombre d'actions proportionnel au nombre que représentent ses actions rapporté au nombre total d’actions détenus au total par les Préempteurs et arrondi au nombre entier inférieur, étant précisé que cette règle sera appliquée en autant de tours que nécessaire jusqu’à affectation de la totalité des actions objet du projet de transfert.

En cas de préemption, la remise des ordres de mouvement portants sur les actions préemptées et le paiement du prix interviendront dans les quinze (15) jours de l’expiration du délai d’exercice du droit de préemption de trente (30) jours susvisé.

En l’absence de préemption, le transfert des actions projeté pourra intervenir régulièrement auprès du cessionnaire envisagé, sous réserve qu’il soit réalisé dans les six (6) mois de l’expiration du délai d’exercice du droit de préemption de trente (30) jours susvisé.

La propriété des actions résulte de leur inscription au nom du ou des titulaires sur des comptes tenus à cet effet par la Société dans les conditions et selon les modalités prévues par la loi.

Les actions ne sont négociables qu'après l'immatriculation de la Société au Registre du Commerce et des Sociétés. En cas d'augmentation du capital, les actions sont négociables à compter de la réalisation de celle-ci.

La transmission des actions s'opère à l'égard de la Société et des tiers par un virement du compte du cédant au compte du cessionnaire, sur production d'un ordre de mouvement. Ce mouvement est préalablement inscrit sur un registre coté et paraphé, tenu chronologiquement dit « registre des mouvements de titres ».

La Société est tenue de procéder à cette inscription et à ce virement dès réception de l'ordre de mouvement.

L'ordre de mouvement, établi sur un formulaire fourni ou agréé par la Société, est signé par le cédant ou son mandataire.

Toutes les cessions effectuées en violation des dispositions du présent article sont nulles.

### Article 12.2 : Rachat des actions par l’entreprise

Tout associé peut demander à l’entreprise d’achater une partie ou l’intégralité de ses actions. Le prix d’achat est le prix d’émission de l’action. L’entreprise
s’engage à les racheter au plus tard cinq (5) années après la demande.

## Article 13 : Dirigeants
### Article 13.1 : Le Président
La Société est dirigée et administrée par un Président personne morale ou personne physique associé ou non de la Société.

Lorsqu'une personne morale est nommée Président, ses dirigeants sont soumis aux mêmes conditions et obligations et encourent les mêmes responsabilités civile et pénale que s'ils étaient Président en leur propre nom, sans préjudice de la responsabilité solidaire de la personne morale qu'ils dirigent.

Les règles fixant la responsabilité des membres du conseil d'administration des sociétés anonymes sont applicables au Président.

En cours de vie sociale, le Président est désigné par une décision collective des associés ou une décision de l'associé unique, le premier Président étant nommé à l’occasion de la signature des statuts constitutifs.

Les associés déterminent la durée du mandat du Président et fixent, le cas échéant, sa rémunération à ce titre, sauf pour le premier président dont la durée du mandat et la rémunération, s’il en est attribuée une, sont fixées statutairement.

Le mandat du Président est renouvelable indéfiniment par décision des associés.

Les fonctions du Président prennent fin
* (i) par l’arrivée du terme de son mandat,
* (ii) par l’incapacité, faillite personnelle ou l’interdiction de gérer,
* (iii) par le décès ou,
* (iv) s’il s’agit d’une personne morale, en cas d'ouverture à son encontre d'une procédure de redressement ou de liquidation judiciaire, d’interdiction de gérer de son Président et en cas de transformation ou de dissolution amiable.

Tout Président peut démissionner de son mandat sous réserve de respecter un préavis de trois (3) mois lequel pourra être réduit lors de la décision collective des associés ou de la décision de l'associé unique qui aura à statuer sur le remplacement du président démissionnaire.

Le Président est révocable à tout moment par une décision des associés ou de l’associé unique.

À défaut de règles particulières qui peuvent être fixées à tout moment par décision des associés ou de l’associé unique en accord avec le Président, la révocation du Président n'a pas à être motivée et ne peut donner lieu à quelque indemnité que ce soit.

Le Président est, à l’égard des tiers, président de la Société au sens de l’article L.227-6 du code de commerce.

Le Président représente la Société à l’égard des tiers. Il est investi des pouvoirs les plus étendus pour agir en toutes circonstances au nom de la Société dans la limite de l’objet social, sous réserve des pouvoirs que la loi et les présents statuts attribuent expressément aux associés, ou à l'associé unique.

La Société est engagée même par les actes du Président qui ne relèvent pas de l'objet social, à moins qu'elle ne prouve que le tiers savait que l'acte dépassait cet objet ou qu'il ne pouvait l'ignorer compte tenu des circonstances, la seule publication des statuts ne suffisant pas à constituer cette preuve.

À tout moment, les pouvoirs du Président peuvent également être limités par décision collective des associés ou par décision de l’associé unique.

Toute limitation des pouvoirs du Président est inopposable aux tiers.

Dans ces limites, le Président peut déléguer certains de ses pouvoirs pour l’exercice de fonctions spécifiques ou l’accomplissement de certains actes à toute personne de son choix, avec ou sans faculté de subdélégation.

### Article 13.2 : Directeurs Généraux et Directeurs Généraux Délégués

En cours de vie sociale et sur proposition du Président, la collectivité des associés ou l’associé unique peut nommer un ou plusieurs Directeurs Généraux ou Directeurs Généraux Délégués, le ou les premiers Directeurs Généraux ou Directeurs Généraux Délégués étant nommés dans les statuts constitutifs, le cas échéant.

Les Directeurs Généraux ou Directeurs Généraux Délégués peuvent être des personnes morales ou des personnes physiques, associées ou non de la Société.

Lorsqu'une personne morale est nommée Directeur Général ou Directeur Général Délégué, ses dirigeants sont soumis aux mêmes conditions et obligations et encourent les mêmes responsabilités civile et pénale que s'ils étaient Directeur Général ou Directeur Général Délégué en leur propre nom, sans préjudice de la responsabilité solidaire de la personne morale qu'ils dirigent.

Les règles fixant la responsabilité des membres du conseil d'administration des sociétés anonymes sont applicables aux Directeurs Généraux et Directeurs Généraux Délégués.

La durée du mandat et la rémunération d’un Directeur Général ou d’un Directeur Général Délégué sont fixées par décision collective des associés ou par décision de l’associé unique, sauf pour le ou les premiers Directeurs Généraux et/ou Directeurs Généraux Délégués dont la durée du mandat et la rémunération, s’il en est attribué une, sont fixées statutairement.

Le mandat d’un Directeur Général ou d’un Directeur Général Délégué est renouvelable indéfiniment par décision des associés.

Les fonctions d’un Directeur Général ou d’un Directeur Général Délégué prennent fin
* (i) par l’arrivée du terme de son mandat,
* (ii) par l’incapacité, faillite personnelle ou l’interdiction de gérer,
* (iii) par le décès ou, (iv) s’il s’agit d’une personne morale, en cas d'ouverture à son encontre d'une procédure de redressement ou de liquidation judiciaire, d’interdiction de gérer de son Président et en cas de transformation ou de dissolution amiable.

Tout Directeur Général ou Directeur Général Délégué peut démissionner de son mandat sous réserve de respecter un préavis de trois (3) mois lequel pourra être réduit lors de la décision collective des associés ou de la décision de l'associé unique qui aura à statuer sur le remplacement du Directeur Général ou Directeur Général Délégué démissionnaire.

Tout Directeur Général ou Directeur Général Délégué est révocable à tout moment par une décision des associés ou de l’associé unique. A défaut de règles particulières qui peuvent être fixées à tout moment par décision des associés ou de l’associé unique en accord avec le Directeur Général ou le Directeur Général Délégué concerné, la révocation n'a pas à être motivée et ne peut donner lieu à quelque indemnité que ce soit.

Les Directeurs Généraux ou Directeurs Généraux Délégués sont investis des mêmes pouvoirs que le Président. Ils représentent la Société à l’égard des tiers et sont investis des pouvoirs les plus étendus pour agir en toutes circonstances au nom de la Société dans la limite de l’objet social, sous réserve des pouvoirs que la loi et les présents statuts attribuent expressément aux associés, ou à l'associé unique.

La Société est engagée même par les actes des Directeurs Généraux ou Directeurs Généraux Délégués qui ne relèvent pas de l'objet social, à moins qu'elle ne prouve que le tiers savait que l'acte dépassait cet objet ou qu'il ne pouvait l'ignorer compte tenu des circonstances, la seule publication des statuts ne suffisant pas à constituer cette preuve.

À tout moment, les pouvoirs des Directeurs Généraux ou Directeurs Généraux Délégués peuvent également être limités par décision collective des associés ou par décision de l’associé unique.

Toute limitation des pouvoirs des Directeurs Généraux ou Directeurs Généraux Délégués est inopposable aux tiers.

Dans ces limites, les Directeurs Généraux ou Directeurs Généraux Délégués peuvent déléguer certains de leurs pouvoirs pour l’exercice de fonctions spécifiques ou l’accomplissement de certains actes à toute personne de leur choix, avec ou sans faculté de subdélégation.

## Article 14 : Décisions collectives
Sont soumises à la décision collective des associés :
* l'approbation des comptes annuels (sociaux et le cas échéant consolidés) et l'affectation du résultat dans les six (6) mois de la clôture de l'exercice social,
* l’émission de valeurs mobilières ou de titres donnant accès au capital de la Société,
* la nomination, le renouvellement et la révocation du Président,
* la nomination, le renouvellement et la révocation des Directeurs Généraux, et des Directeurs Généraux Délégués,
* la fixation de la rémunération du Président, des Directeurs Généraux, et des Directeurs Généraux Délégués,
* la fixation de règles particulières applicables à la révocation du Président et/ou du ou des Directeurs Généraux et/ou du ou des Directeurs Généraux Délégués,
* la nomination des commissaires aux comptes,
* l’approbation des conventions conclues visées à l’article 15 des Statuts,
* l'extension ou la modification de l'objet social,
* l'augmentation, la réduction ou l'amortissement du capital,
* la fusion, la scission de la Société, la transmission universelle du patrimoine ou les apports partiels d'actifs réalisés par (ou au profit de) la Société,
* la prorogation de la durée de la Société,
* la décision de poursuivre ou non l’activité de la Société dans l’hypothèse où les capitaux propres deviennent inférieurs à la moitié du capital social,
* la dissolution ou la liquidation de la Société,
* l’augmentation des engagements d’un associé, et
* plus généralement, toute décision ayant pour effet ou pour objet de modifier, directement ou indirectement les Statuts sauf lorsque cette compétence a été explicitement dévolue au Président ou à un Directeur Général, ou à un Directeur Général Délégué.

Sont soumises à une décision à l’unanimité des associés en application des dispositions légales applicables :
* la transformation de la Société ;
* l’adoption ou la modification de toute clause prévoyant
 * (i) l’inaliénabilité temporaire des actions,
 * (ii) la nécessité d’un agrément en cas de cession d’actions,
 * (iii) la possibilité d’exclure un associé ou
  * (iv) des règles particulières en cas de changement du contrôle d’une société associée ;
* toute décision ayant pour effet d’augmenter les engagements des associés de la Société.

Toutes les autres décisions relèvent de la compétence du Président, ou des Directeurs Généraux et des Directeurs Généraux Délégués.

### Article 14.1 : Fréquence des décisions collectives
Les associés sont appelés à prendre une décision collective au moins une fois par an (dans les six (6) mois de la clôture de l'exercice social) à l'effet d'approuver les comptes sociaux (et le cas échéant, les comptes consolidés) de l'exercice social écoulé.

Les autres décisions collectives sont prises à toute époque de l'année.

### Article 14.2 : Modalités des décisions collectives
Les décisions collectives des associés sont prises à l'initiative du Président, d’un Directeur Général, d’un Directeur Général Délégué ou d’un ou plusieurs associés détenant seul ou ensemble plus de 5 % du capital social et des droits de vote de la Société (le « Demandeur »).

Les décisions de quelque nature qu'elles soient, sont prises, au choix du Demandeur, soit en assemblée générale, soit par consultation écrite, soit dans un acte sous seing privé signé par tous les associés.

### Article 14.3 : Assemblées générales
La réunion d'une assemblée générale est facultative.
L'assemblée générale est convoquée par le Demandeur, huit (8) jours avant la date de la réunion, par tous moyens mentionnant le jour, l'heure, le lieu et l'ordre du jour de la réunion.

Toutefois, lorsque tous les associés sont présents ou représentés, l'assemblée se réunit valablement sur convocation verbale et sans délai, sous réserve du droit à l’information préalable du commissaire aux comptes et du comité d’entreprise. Le Demandeur adresse aux associés les documents nécessaires à leur information.

L’assemblée générale peut se tenir en tout lieu indiqué dans la convocation (au siège social ou tout lieu en France ou à l’étranger).

L'assemblée est présidée par le Président, à défaut elle élit son Président. A chaque assemblée est tenue une feuille de présence et il est dressé un procès-verbal de la réunion tel qu’indiqué ci-dessous.

Chaque associé a le droit de participer aux décisions collectives par lui-même ou par un mandataire de son choix, qui peut ou non être un associé. Les mandats peuvent être donnés par tous moyens de communication écrite (en ce compris la télécopie et le courriel).

La réunion peut être organisée par téléconférence téléphonique ou audiovisuelle.

Dans tous les cas, le Demandeur établit dans un délai de sept (7) jours à compter de l’assemblée générale, un projet du procès-verbal de séance après avoir indiqué :
* le mode de consultation ;
* le lieu, la date et l’heure de l’assemblée générale ;
* l’identité des associés présents ou représentés ou absents, en précisant, le cas échéant, les mandats donnés à cet effet et s’ils étaient physiquement présents ou intervenaient par téléconférence. Dans cette hypothèse, les mandats sont annexés au procès-verbal ;
* la liste des documents et rapports transmis aux associés ;
* un exposé des débats ;
* le texte des résolutions mises aux voix et le résultat des votes.

Dans un délai de sept (7) jours à compter de son expédition, les associés ayant pris part à l’assemblée en retournent une copie après l’avoir signée, par tous moyens de communication écrite (en ce compris la télécopie et le courriel). En l’absence d’observations dans ce délai, le défaut de signature vaudra acceptation par l’associé concerné du texte du procès-verbal.

Le Demandeur établit alors le procès-verbal définitif. Ledit procès-verbal dûment signé par le Demandeur, ainsi que la preuve de l’envoi du procès-verbal aux associés et les copies renvoyées dûment signées par les associés ainsi qu’il est indiqué ci-dessus sont immédiatement communiqués à la Société pour être conservés.

### Article 14.4 : Consultation écrite
En cas de consultation par correspondance (en ce compris toute consultation effectuée par télécopie ou par transmission électronique), le texte des résolutions proposées (et un bulletin de vote correspondant) ainsi que les documents nécessaires à l’information des associés sont adressés par le Demandeur à chaque associé, par tous moyens de communication écrite (en ce compris la télécopie et le courriel).

Les associés disposent d’un délai de quinze (15) jours à compter de la réception des projets de résolutions, pour adresser leur vote (et le bulletin de vote) au Demandeur. Le vote peut être émis par tous moyens de communication écrite (en ce compris la télécopie et le courriel). Si les votes de tous les associés sont reçus avant l’expiration dudit délai, la ou les résolution(s) concernée(s) sera(ont) réputée(s) avoir fait l’objet d’un vote à la date de réception du dernier vote.

La décision collective des associés fait l'objet d'un procès-verbal établi et signé par le Demandeur (en annexe duquel figurent chacune des réponses reçues des associés) contenant les indications suivantes :
* le mode de consultation ;
* l’identité des associés ayant répondu ;
* le texte des résolutions et le résultat du vote pour chaque résolution proposée ;
* la liste des documents et rapports transmis aux associés.
Ce procès-verbal est immédiatement communiqué à la Société pour être conservé.

### Article 14.5 : Acte sous seing privé
La décision des associés peut aussi s’exprimer sous forme d’un acte sous seing privé signé par tous les associés.

Les associés peuvent consentir un mandat à toute personne de leur choix (associé ou non) pour signer l’acte en question.

Cette décision collective des associés fait l'objet d'un procès-verbal établi et signé par tous les associés contenant les indications suivantes :
le mode de consultation ;
* l’identité des associés signant l’acte ;
* le texte des résolutions et la décision des associés correspondant ; et
la liste des documents et rapports transmis aux associés.

Ce procès-verbal est immédiatement communiqué à la Société pour être conservé.


### Article 14.6 : Droit d’information des associés
Quel que soit le mode de consultation, toute décision des associés doit faire l'objet d'une information préalable comprenant l'ordre du jour, le texte des résolutions et tous documents et informations leur permettant de se prononcer en connaissance de cause sur la ou les résolutions soumises à leur approbation.

Les rapports établis par le Président (ainsi que tout document d’information) doivent être communiqués, aux frais de la Société, aux associés, huit (8) jours avant la date de la consultation.

### Article 14.7 : Règles de majorité
Sous réserve des dispositions légales applicables (notamment les articles L.227-19 et L.227-3 du code de commerce) qui exigent l'unanimité des associés, les décisions collectives sont prises à la majorité simple des voix dont disposent les associés présent ou représentés (ou ayant retourné le bulletin de vote en cas de consultation écrite).

## Article 15 : Conventions entre la Société et ses dirigeants ou associés
Le commissaire aux comptes ou, s’il n’en a pas été désigné, le Président présente aux associés un rapport sur les conventions intervenues directement ou par personne interposée entre la Société et son Président, l’un de ses dirigeants, l’un de ses actionnaires disposant d’une fraction des droits de vote supérieure à 10% ou, s’il s’agit d’une société actionnaire, la société la contrôlant au sens de l’article L. 233-3 du code de commerce.

Les associés statuent sur ce rapport et approuvent les conventions visées ci-dessus.

Les conventions non approuvées produisent néanmoins leurs effets, à charge pour la personne intéressée et éventuellement pour le Président et les autres dirigeants d’en supporter les conséquences dommageables pour la Société.

Par dérogation aux stipulations précitées, lorsque la Société ne comprend qu’un seul associé, il est seulement fait mention au registre des décisions des conventions intervenues directement ou par personnes interposées entre la Société et son dirigeant.

## Article 16 : Comptes annuels
Il est tenu une comptabilité régulière des opérations sociales conformément à la loi.

À la clôture de chaque exercice, le Président établit l’inventaire, les comptes annuels sociaux (et le cas échéant consolidés) et le rapport de gestion conformément aux lois et usages du commerce.

Il les soumet pour approbation à la collectivité des associés ou à l’associé unique dans le délai de six (6) mois à compter de la date de clôture de l'exercice.

## Article 17 : Affectation et répartition du résultat
Le compte de résultat qui récapitule les produits et charges de l’exercice, fait apparaître par différence, après déduction des amortissements et des provisions, le bénéfice ou la perte de l’exercice.

Sur le bénéfice de l’exercice diminué, le cas échéant, des pertes antérieures, il est prélevé cinq pour cent au moins pour constituer le fond de réserve légale. Ce prélèvement cesse d’être obligatoire lorsque le fonds de réserve atteint le dixième du capital social ; il reprend son cours lorsque, pour une cause quelconque, la réserve légale est descendue au dessous de ce dixième.

Si les comptes de l'exercice, approuvés par une décision collective des associés ou une décision de l'associé unique, font apparaître un bénéfice distribuable tel qu'il est défini par la loi, la collectivité des associés ou l'associé unique peut décider de l'inscrire à un ou plusieurs postes de réserves dont elle/il règle l'affectation ou l'emploi, de le reporter à nouveau ou de le distribuer sous forme de dividendes.

Hors le cas de réduction du capital, aucune distribution ne peut être faite aux associés lorsque les capitaux propres sont ou deviendraient à la suite de celle-ci inférieurs au montant du capital augmenté des réserves que la loi ou les statuts ne permettent pas de distribuer.

Les modalités de mise en paiement des dividendes en numéraire sont fixées par la décision de la collectivité des associés ou de l’associé unique ou, à défaut par le Président.

La mise en paiement des dividendes, en numéraire doit avoir lieu dans un délai maximal de neuf mois après la clôture de l’exercice, sauf prorogation de ce délai par décision de justice.

La collectivité des associés ou l'associé unique peut accorder pour tout ou partie du dividende mis en distribution ou des acomptes sur dividende, une option entre le paiement du dividende en numéraire ou en actions dans les conditions légales.

Les pertes, s'il en existe, sont après l'approbation des comptes par la collectivité des associés ou par l'associé unique, soit imputées sur les comptes de réserves de la Société soit reportées à nouveau pour être imputées sur les bénéfices des exercices ultérieurs jusqu'à extinction.

## Article 18 : Capitaux propres inférieurs à la moitié du capital social
Si, du fait des pertes constatées dans les documents comptables, les capitaux propres de la Société deviennent inférieurs à la moitié du capital social, le Président est tenu, dans les quatre mois qui suivent l'approbation des comptes ayant fait apparaître ces pertes, de provoquer une décision collective des associés ou de l'associé unique, à l'effet de décider s'il y a lieu à dissolution anticipée de la Société.

Si la dissolution n'est pas prononcée, le capital doit être, sous réserve des dispositions légales relatives au capital minimum dans les sociétés anonymes, et dans le délai fixé par la loi, réduit d'un montant égal à celui des pertes qui n'ont pu être imputées sur les réserves si dans ce délai les capitaux propres ne sont pas redevenus au moins égaux à la moitié du capital social.

## Article 19 : Commissaires aux comptes
Pour le cas où la Société réunit les conditions visées par l’article L.227-9-1 du Code de commerce, le contrôle de la Société est effectué par un ou plusieurs commissaires aux comptes titulaires, nommés pour une durée de six (6) exercices et exerçant leur mission conformément à la loi.

Un ou plusieurs commissaires aux comptes suppléants appelés à remplacer le ou les titulaires en cas de refus, d'empêchement, de démission ou de décès, sont nommés en même temps que le ou les titulaires pour la même durée.

Le commissaire aux comptes est avisé de la consultation des associés en même temps que les associés et selon les mêmes formes. Il est avisé de l’ordre du jour de la consultation et reçoit sur sa demande, l’ensemble des informations destinées aux associés. Le commissaire aux comptes peut communiquer aux associés ou à l’associé unique ses observations sur les questions mises à l’ordre du jour ou sur toute autre question relevant de sa compétence, par écrit en cas de décision unanime. Le commissaire aux comptes est convoqué à toutes les assemblées.

## Article 20 : Comité d’entreprise
Lorsqu’il a été constitué un comité d’entreprise, les délégués de ce comité, désignés conformément aux dispositions du code du travail, exercent leurs droits définis à l’article L.2323-66 du code du travail auprès du Président.

Le comité d’entreprise est informé des décisions collectives des associés en même temps et selon les mêmes formes que les associés.

## Article 21 : Transformation
La Société peut se transformer en société de toute autre forme par une décision unanime des associés.

## Article 22 : Dissolution – Liquidation
La Société est dissoute à l'arrivée du terme statutaire de sa durée, sauf prorogation régulière, ou s'il survient une cause de dissolution prévue par la loi.

La dissolution de la Société peut également être prononcée dans les conditions du droit commun applicables aux sociétés anonymes dans le cas où les capitaux propres de la Société deviendraient inférieurs à la moitié du montant du capital social.

Si la Société ne comprend qu'un seul associé personne morale, la dissolution pour quelque cause que ce soit entraîne la transmission universelle du patrimoine à l'associé unique, sans qu'il ait lieu à liquidation.

Si au jour de la dissolution, la Société comprend au moins deux associés, la dissolution, pour quelque cause que ce soit, entraîne sa liquidation. Cette liquidation est effectuée dans les conditions et selon les modalités prévues par les dispositions légales et réglementaires en vigueur au moment de son ouverture.

La dissolution met fin aux fonctions des dirigeants ; le commissaire aux comptes conserve son mandat sauf décision contraire des associés ou de l'associé unique.

## Article 23 : Contestations
Toutes les contestations qui, pendant la durée de la Société ou lors de sa liquidation, s'élèveraient soit entre la Société et les associés, soit entre les associés eux-mêmes à propos des affaires sociales, seront soumises à la juridiction des tribunaux compétents du siège social.

Fait à Paris

Date de signature : 17/07/2016

Codeurs en Liberté

Société par actions simplifiée à capital variable

Capital : 4000 euros

Siège social : 19 A Square de Monsoreau 75020 Paris

Société par actions simplifiée à capital variable
