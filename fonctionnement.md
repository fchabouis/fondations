# Préambule

Codeurs en Liberté est une entreprise accueillant des salariés indépendants.

Chaque employé est libre de travailler pour les clients et les sujets qui
l’intéressent, voire de ne pas travailler à condition de respecter les
engagements envers l’entreprise.

## Engagements de l’entreprise

Codeurs en Liberté embauche le salarié en CDI à temps partiel de 30 heures
par semaine avec un salaire de 15 000€ bruts par an. Ce contrat s’accompagne
d’une protection sociale (salaire versé dès le 1er jour d’arrêt maladie,
  mutuelle…).

L’employé est ainsi couvert des accidents de la vie, et l’entreprise
compense la protection défaillante qui devrait relever de la société.

L’essentiel des revenus issus du travail de l’employé lui sont reversés sous la
forme de primes si ceux dépassent le salaire miminal. Le détail de la
répartition des revenus est détaillé plus bas.

## Engagement du salarié

Le salarié doit effectuer par lui même le démarchage et la négociations des
contrats. À ce titre, il s’engage à trouver et réaliser des projets pour un
chiffre d’affaires minimal de 6000€ chaque trimestre.

De plus, chaque salarié s’engage à prendre au moins une part sociale dans
l’entreprise et d’exercer les droits de vote associés.

# Répartition des revenus

Pour chaque mission, l’entreprise prélève :
* 5% destinés à la gestion courante de l’entreprise ;
* 5% destinés à alimenter un compte de solidarité.

Ensuite, les frais liés au projet (déplacements, repas, ordinateur…)
sont déduits.

La montant restant est entièrement consacrée à la paye du salarié.

Afin de gérer la variabilité des activités, la masse est répartie sur les
trois mois à partir de la facturation. Sur demande de l’employé et avec
l’accord des autres employés, la durée de la répartition peut être réduite
(activité récurrente garantie) ou étalée (projet personnel de longue durée).

# Obligations du porteur de projet

Le rôle du porteur de projet est d’apporter un contrat et d’assurer le travail
administratif qui y est associé. Dans le cas général, le porteur de projet est
également celui qui réalise le projet.

Il est le lien entre l’entreprise et le client. À ce titre il doit s’assurer
de la bonne réalisation du projet, de l’édition des factures, de l’encaissement
et de la bonne répartition des revenus.

Si les personnes qui apportent le projet, gèrent le travail administratif et
réalisent le projet sont différentes, la répartition de la rémunération est
définie en amont, au cas par cas.

# Utilisation du compte de solidarité

Ce compte sert à maintenir le salaire de base en cas de creux d’activité,
de maladie, ou sur décision collective à financer une activité non
rémunérée.

À la cloture du bilan, les sommes restantes abondées sur ce compte sont
considérés comme des bénéfices. Ils sont alors distribués de manière à être
compatible avec le status de coopérative :

* 1/3 en salaire ;
* 1/3 en dividendes ;
* 1/3 en provisions pour l’année suivantes.

La répartition exacte est décidée en assemblée générale.
